const connect = require("../db/connect");

async function cleanUpSchedules() {
    const currentDate = new Date();

    currentDate.setDate(currentDate.getDate() - 7) //Manupilação da data, para que com 7 dias das reservas, elas sejam deletadas
    const formattedDate = currentDate.toISOString().split("T")[0]; //Formata a data para YYYY-MM-DD

    const query = `DELETE FROM schedule WHERE dateEnd < ?`
    const values = [formattedDate];

    return new Promise((resolve, reject) => {
        connect.query(query, values, function (err, results) {
            if (err) {
                console.error("Erro MYSQL", err);
                return reject(new Error("Erro ao limpar agendamentos"));
            }
            console.log("Agendamentos antigos foram deletados");
            resolve("Agendamentos antigos deletados com sucesso")
        });
    });
}

module.exports = cleanUpSchedules;