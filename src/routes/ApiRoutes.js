const router = require("express").Router();
const dbController = require("../controller/dbController");
const clienteController = require("../controller/clienteController");
const pizzariaController = require("../controller/pizzariaController");
const exercicioController = require("../controller/exercicioController");

router.get("/tabelasNames", dbController.getAllTableNames)
router.get("/tabelasDescriptions", dbController.getAllTableDescriptions)

router.post("/postcliente", clienteController.createCliente)

router.get("/clientes", clienteController.getAllClientes)
router.get("/clientesWhere", clienteController.getAllClientesWhere)

router.get("/listarPedidosPizza", pizzariaController.listarPedidosPizzas)
router.get("/getPedidosPizza", exercicioController.getPedidosPizza)
module.exports = router;
