const express = require('express')
const app = express();
const cors = require('cors');
require('./cron');

class AppController {
    constructor() {
      this.express = express();
      this.middlewares();
      this.routes();
    }

    middlewares() {
      this.express.use(express.json());
      this.express.use(cors());
    }

    routes() {
      const apiRoutes= require('./routes/ApiRoutes');
      this.express.use('/', apiRoutes);

    }
  }

  module.exports = new AppController().express;