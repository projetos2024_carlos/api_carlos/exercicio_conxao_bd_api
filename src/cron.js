const cron = require("node-cron");
const cleanUpSchedules = require("./services/cleanUpSchedulesServices");

//Agendamento da limpeza

cron.schedule("0 0  * * *", async()=>{
    try {
        await cleanUpSchedules();
        console.log("Limpeza automática realizada!");
    } catch (error) {
        console.error("Erro ao executar limpeza automática", error);
    }
});