const connect = require('../db/connect');

// Função para recuperar todos os nomes das tabelas
async function getAllTableNames(req, res) {
    try {
        const queryShowTables = "SHOW TABLES";
        connect.query(queryShowTables, function(err, result, fields) {
            if (err) {
                console.log(err);
                return res.status(500).json({ error: "Erro ao obter tabelas do banco de dados" });
            }
            const tableNames = result.map(row => row[fields[0].name]);
            res.status(200).json({ tableNames });
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({ error: "Erro interno do servidor" });
    }
}

// Função para recuperar todas as descrições das tabelas e seus atributos
async function getAllTableDescriptions(req, res) {
    try {
        const queryShowTables = "SHOW TABLES";
        connect.query(queryShowTables, async function(err, result, fields) {
            if (err) {
                console.log(err);
                return res.status(500).json({ error: "Erro ao obter tabelas do banco de dados" });
            }
            const tables = [];
            for (let i = 0; i < result.length; i++) {
                const tableName = result[i][`Tables_in_${connect.config.connectionConfig.database}`];
                const queryDescTable = `DESCRIBE ${tableName}`;
                try {
                    const tableDescription = await new Promise((resolve, reject) => {
                        connect.query(queryDescTable, function(err, result, fields) {
                            if (err) {
                                reject(err);
                            }
                            resolve(result);
                        });
                    });
                    tables.push({ name: tableName, description: tableDescription });
                } catch (error) {
                    console.log(error);
                    return res.status(500).json({ error: "Erro ao obter a descrição da tabela!" });
                }
            }
            res.status(200).json({ tables });
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({ error: "Erro interno do servidor" });
    }
}


module.exports = {
    getAllTableNames,
    getAllTableDescriptions
};