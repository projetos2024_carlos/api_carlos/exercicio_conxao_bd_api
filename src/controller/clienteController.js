const connect = require("../db/connect"); //Estabilizando uma conexão com o banco de dados

module.exports = class clienteController {
    static async createCliente(req, res) {
        const {
            telefone,
            nome,
            cpf,
            logradouro,
            numero,
            complemento,
            bairro,
            cidade,
            estado,
            cep,
            referencia,
        } = req.body;

        if (telefone !== 0) {
            const query = `insert into cliente (telefone, nome, cpf, logradouro, numero, complemento, bairro, cidade, estado, cep, referencia) values (
                '${telefone}',
                '${nome}',
                '${cpf}',
                '${logradouro}',
                '${numero}',
                '${complemento}',
                '${bairro}',
                '${cidade}',
                '${estado}',
                '${cep}',
                '${referencia}'
        )`;
            try {
                connect.query(query, function (err) {
                    if (err) {
                        console.log(err);
                        res.status(500).json({ error: "Usuario não cadastrado no banco!" });
                        return;
                    }
                    console.log("Inserido no banco");
                    res.status(201).json({ message: "usuário criado com sucesso!" });
                });
            } catch (error) {
                console.error("Erro ao executar o insert!", error);
                res.status(500).json({ error: "Erro interno do servidor!" });
            }
        }
        else {
            res.status(400).json({ message: "O telefone é obrigatório" })
        }
    }

    static async getAllClientes(req, res) {
        const query = `select * from cliente`;

        try {
            connect.query(query, function (err, data) {
                if (err) {
                    console.log(err);
                    res.status(500), json({ error: "Usuários não encontrados no banco!" });
                    return;
                }
                let clientes = data;
                console.log("Consulta realizada com sucesso!");
                res.status(201).json({ clientes });
            });
        } catch (error) {
            console.log("Erro ao executar a consulta: ", error);
            res.status(500).json({ error: "Erro interno de servidor!" })
        }
    }
    static async getAllClientesWhere(req, res) {

        const { filtro, ordenacao, ordem } = req.query;
        let query = `select * from cliente`; //Deve ser let por conta da alteração caso haja filtro

        if (filtro) {
            // query = query + filtro; -- Está incorreto

            // query = query + ` where ${filtro}` ;

            query += ` where ${filtro}`; // Versão simplificada e mais utilizada do comando acima
        }
        //Adicionar a cláusula order by quando a mesma existir
        if (ordenacao) {
            query += `order by ${ordenacao}`;

            //Adicionar a ordem do order by (asc ou desc)
            if (ordem) {
                query += `${ordem}`;
            } //Fim do if
        } //Fim do if ordenação

        try {
            connect.query(query, function (err, result) {
                if (err) {
                    console.log(err);
                    res.status(500), json({ error: "Usuários não encontrados no banco!" });
                    return;
                }
                
                console.log("Consulta realizada com sucesso!");
                res.status(201).json({ result });
            });
        } catch (error) {
            console.log("Erro ao executar a consulta: ", error);
            res.status(500).json({ error: "Erro interno de servidor!" })
        }
    }
}